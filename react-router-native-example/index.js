import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { NativeRouter, Route, Link } from "react-router-native";

import TabBarIcon from '../components/TabBarIcon';

function Home() {
  return <Text style={styles.header}>Home</Text>;
}

function About() {
  return <Text style={styles.header}>About</Text>;
}

function VeryImportant({ match }) {
  return <Text style={styles.topic}>VeryImportant</Text>;
}


const ReactRouterNativeExample = () => (
  <NativeRouter>
    <View style={styles.container}>
      <View style={styles.nav}>
        <Link to="/" underlayColor="#f0f4f7" style={styles.navItem}>
          <Text>Home</Text>
        </Link>
        <Link to="/about" underlayColor="#f0f4f7" style={styles.navItem}>
          <Text>About</Text>
        </Link>
        <Link to="/veryImportant" underlayColor="#f0f4f7" style={styles.navItem}>
          <Text>veryImportant</Text>
        </Link>
      </View>

      <Route exact path="/" component={Home} />
      <Route path="/about" component={About} />
      <Route path="/veryImportant" component={VeryImportant} />
    </View>
  </NativeRouter>
)

const styles = StyleSheet.create({
  container: {
    marginTop: 25,
    padding: 10
  },
  header: {
    fontSize: 20
  },
  nav: {
    flexDirection: "row",
    justifyContent: "space-around"
  },
  navItem: {
    flex: 1,
    alignItems: "center",
    padding: 10,
    backgroundColor: 'rgba(0, 0, 0, 0.1)'
  },
  subNavItem: {
    padding: 5
  },
  topic: {
    textAlign: "center",
    fontSize: 15
  }
});

export default ReactRouterNativeExample;