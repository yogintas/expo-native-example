import './scripts';
import { AppLoading } from 'expo';
import { Asset } from 'expo-asset';
import * as Font from 'expo-font';
import React, { useState, useEffect } from 'react';
import { Platform, StatusBar, StyleSheet, View, Text, Button } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { database } from './api'

import AppNavigator from './navigation/AppNavigator';
import ReactRouterNativeExample from './react-router-native-example'

export default function App(props) {
  const [isLoadingComplete, setLoadingComplete] = useState(false);
  const [isExampleShown, setIsExampleShown] = useState(false);
  const [entities, setEntities] = useState([]);

  useEffect(() => {
    const getEntities = async () => {
      const res = await database.entities.get();

      setEntities(res);
    }

    getEntities();
  }, [])

  if (!isLoadingComplete && !props.skipLoadingScreen) {
    return (
      <AppLoading
        startAsync={loadResourcesAsync}
        onError={handleLoadingError}
        onFinish={() => handleFinishLoading(setLoadingComplete)}
      />
    );
  } else {
    return (
      <View style={styles.container}>
        <Text style={{ paddingTop: '10%', textAlign: 'center' }}>¯\_(ツ)_/¯ App.js</Text>
        <Text style={{ paddingTop: '10%', textAlign: 'center' }}>{entities.map(({ name }) => name)}</Text>
        <StatusBar barStyle="default" />
        <Button style={{ marginTop: '100%' }} onPress={() => setIsExampleShown(!isExampleShown)} title="Switch to react-router-native-example" />
        <Text style={{ paddingTop: '10%', textAlign: 'center' }}>{`isExampleShown: ${isExampleShown}`}</Text>
        {isExampleShown ? <ReactRouterNativeExample /> : <AppNavigator />}
        {/* <AppNavigator /> */}
      </View>
    );
  }
}

async function loadResourcesAsync() {
  await Promise.all([
    Asset.loadAsync([
      require('./assets/images/robot-dev.png'),
      require('./assets/images/robot-prod.png'),
    ]),
    Font.loadAsync({
      // This is the font that we are using for our tab bar
      ...Ionicons.font,
      // We include SpaceMono because we use it in HomeScreen.js. Feel free to
      // remove this if you are not using it in your app
      'space-mono': require('./assets/fonts/SpaceMono-Regular.ttf'),
    }),
  ]);
}

function handleLoadingError(error) {
  // In this case, you might want to report the error to your error reporting
  // service, for example Sentry
  console.warn(error);
}

function handleFinishLoading(setLoadingComplete) {
  setLoadingComplete(true);
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});
