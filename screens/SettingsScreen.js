import React from 'react';
import { Text } from 'react-native';
import { ExpoConfigView } from '@expo/samples';


export default function SettingsScreen() {
  /**
   * Go ahead and delete ExpoConfigView and replace it with your content;
   * we just wanted to give you a quick view of your config.
   */
  return (
    <>
      <Text style={{ paddingTop: 10, textAlign: 'center' }}>¯\_(ツ)_/¯ SettingzzScreen.js</Text>
      <ExpoConfigView />
    </>
  )
}

SettingsScreen.navigationOptions = {
  title: 'app.json',
};
